﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scrDebug : MonoBehaviour
{
    public Text debugText;
    public float refreshTime;
    public Color frameColorGreen, frameColorRed;

    private int frameCounter;
    private float timeCounter;
    private float lastFramerate;

    private Color frameColorCurrent;

    void Update()
    {
        debugText.gameObject.SetActive(scrLevel.instance.debug);
        if (!scrLevel.instance.debug) return;

        // calculate framerate
        if (timeCounter < refreshTime)
        {
            timeCounter += Time.deltaTime;
            frameCounter++;
        }
        else
        {
            lastFramerate = frameCounter / timeCounter;
            frameCounter = 0;
            timeCounter = 0;
        }

        // decide FPS indicator color
        frameColorCurrent = Color.Lerp(frameColorRed, frameColorGreen, lastFramerate / 60);
        var frameColorString = $"<color=#{ColorUtility.ToHtmlStringRGB(frameColorCurrent)}>";

        // set debug text
        var music = scrMusic.instance;

        debugText.text =
            $"{frameColorString}FPS: {Mathf.RoundToInt(lastFramerate)}</color>" +
            $"\n<size=22>\nTotal beat: {Mathf.FloorToInt(music.beat)}" +
            $"\nCrotchet: {music.crotchet:F2}" + $"\nBar: {music.bar}</size>";

        // shortcuts
        if (Input.GetKeyDown(KeyCode.Period))
        {
            var newTime = ((music.bar + 1) * music.crotchetsPerBar) * (60 / music.bpm);
            music.GetComponent<AudioSource>().time = newTime;
        }

        if (Input.GetKeyDown(KeyCode.Comma))
        {
            var newTime = ((music.bar - 1) * music.crotchetsPerBar) * (60 / music.bpm);
            newTime = Mathf.Clamp(newTime, 0, newTime); // prevent rewinding too much
            music.GetComponent<AudioSource>().time = newTime;
        }

        if (Input.GetMouseButton(1))
        {
            Time.timeScale = 0.5f;
            music.GetComponent<AudioSource>().pitch = 0.5f;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            Time.timeScale = 1f;
            music.GetComponent<AudioSource>().pitch = 1f;
        }

        if (Input.GetMouseButton(4))
        {
            Time.timeScale = 2f;
            music.GetComponent<AudioSource>().pitch = 2;
        }
        else if (Input.GetMouseButtonUp(4))
        {
            Time.timeScale = 1f;
            music.GetComponent<AudioSource>().pitch = 1f;
        }
    }
}
