﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrPlayer : MonoBehaviour
{
    public int health;
    public float moveSpeed;

    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        var velocity = new Vector2();
        if (scrSettings.instance.GetKey(KeyBind.Up))
        {
            velocity.y = 1;
        }
        else if (scrSettings.instance.GetKey(KeyBind.Down))
        {
            velocity.y = -1;
        }

        if (scrSettings.instance.GetKey(KeyBind.Left))
        {
            velocity.x = -1;
        }
        else if (scrSettings.instance.GetKey(KeyBind.Right))
        {
            velocity.x = 1;
        }

        GetComponent<Rigidbody2D>().velocity = velocity.normalized * moveSpeed;
    }
}
