﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrMusic : SingletonBehaviour<scrMusic>
{
    [Header("Configuration")]
    public string songDirectory;
    public string songFilename;
    public bool isInternalSong;
    public float bpm;
    public int offset;
    public int crotchetsPerBar;
    public float volume;

    [Header("Important")]
    public float bar;
    public float beat;
    public float crotchet;
    public float time;
    public float sampleTime;
    public float frequency;

    [NonSerialized]
    public double startTime;
    private AudioSource source;

    void Update()
    {
        // beat math
        //time = source.time - offset / 1000;
        //time = source.timeSamples / frequency;
        time = (float) (AudioSettings.dspTime - startTime);
        beat = time / (60 / bpm);
        bar = Mathf.FloorToInt(beat / crotchetsPerBar);
        crotchet = beat - (bar * crotchetsPerBar);

        // update volume
        source.volume = volume / 1000;
    }

    //loading the song takes time, so make sure to call this before calling Play()
    //otherwise events at the very beginning might be delayed slightly???
    public void Load()
    {
        if (string.IsNullOrEmpty(songFilename)) return;

        source = GetComponent<AudioSource>();

        if (scrLevel.instance.isInternalLevel)
        {
            var songPath = "Sounds/Music/" + songFilename;
            source.clip = Resources.Load<AudioClip>(songPath);
        }
        else
        {
            if (string.IsNullOrEmpty(songDirectory)) return;
        }
        frequency = source.clip.frequency;
    }

    public void Play()
    {
        startTime = AudioSettings.dspTime;
        if (offset > 0)
        {
            source.Play();
            source.time = offset / 1000;
        }
        else
        {
            source.PlayDelayed(-offset / 1000);
        }
    }
}
