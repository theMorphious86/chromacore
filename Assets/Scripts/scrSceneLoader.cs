﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scrSceneLoader : SingletonBehaviour<scrSceneLoader>
{
    public static string sceneToLoad = "scnLevel";

    public static string levelToLoad = "level_test";
    public static bool isInternalLevel = false;

    public void LoadScene(string scene)
    {
        sceneToLoad = scene;
        SceneManager.LoadScene("scnLoading");
    }

    public void LoadLevel(string level, bool isInternal = true)
    {
        levelToLoad = level;
        isInternalLevel = isInternal;
        LoadScene("scnLevel");
    }

    void OnEnable()
    {
        Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (SceneManager.GetActiveScene().name == "scnLevel")
        {
            scrLevel.instance.levelFilename = levelToLoad;
            scrLevel.instance.RunLevel();
        }
    }
}
