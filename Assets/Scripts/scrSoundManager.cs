﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrSoundManager : SingletonBehaviour<scrSoundManager>
{
    public GameObject soundPrefab;
    public GameObject soundContainerPrefab;

    private const string containerName = "SoundContainer";

    public Sound NewSoundAt(GameObject obj, AudioClip audioClip, bool global = false)
    {
        GameObject soundContainer = GameObject.Find(GetGameObjectPath(obj) + "/" + containerName);

        // make sound container if it doesn't already exist
        if (soundContainer == null)
        {
            soundContainer = Instantiate(soundContainerPrefab, obj.transform);
            soundContainer.transform.parent = obj.transform;
            soundContainer.transform.position = obj.transform.position;
            soundContainer.name = containerName;
        }

        // instantiate audio obj
        GameObject audioSourceObj = Instantiate(soundPrefab, soundContainer.transform);
        audioSourceObj.name = audioClip.name;
        if (global)
        {
            audioSourceObj.GetComponent<AudioSource>().spatialBlend = 0;
        }

        var sound = audioSourceObj.GetComponent<Sound>();
        sound.audioClip = audioClip;

        return sound;
    }

    public Sound NewSound(AudioClip audioClip)
    {
        return NewSoundAt(Camera.main.gameObject, audioClip, true);
    }

    public static string GetGameObjectPath(GameObject obj)
    {
        string path = "/" + obj.name;
        while (obj.transform.parent != null)
        {
            obj = obj.transform.parent.gameObject;
            path = "/" + obj.name + path;
        }
        return path;
    }
}
