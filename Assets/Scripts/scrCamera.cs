﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrCamera : MonoBehaviour
{
    public Transform player;
    public float lerpAmount;
    public float snipeDistance;

    private Vector2 newPos;

    void Update()
    {
        var playerPos = player.position;
        var mousePos = GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition) - playerPos;
        var offset = mousePos * snipeDistance;
        newPos = Vector2.Lerp(newPos, playerPos + offset, lerpAmount * Time.deltaTime * 60);

        transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
    }
}
