using System;
using System.Dynamic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TinyJson;

public class scrLevel : SingletonBehaviour<scrLevel>
{
    public bool debug;
    public bool paused;
    public float levelSpeed;
    public bool isInternalLevel;
    public string levelDirectory;
    public string levelFilename;

    [NonSerialized]
    public Dictionary<string, object> patterns;
    private int lastPreExecuteBar = -1;
    private LinkedList<EventBase> events = new LinkedList<EventBase>();

    void Start()
    {

    }

    void Update()
    {
        while (events.Count > 0 && scrMusic.instance.time >= events.First.Value.time)
        {
            var ev = events.First.Value;
            events.RemoveFirst();
            ev.Run();
        }

        if (lastPreExecuteBar < scrMusic.instance.bar)
        {
            lastPreExecuteBar++;
            PreExecuteNextBarEvents();
        }
    }

    public void RunLevel()
    {
        scrMusic.instance.Load();
        string levelFile = "";
        if (isInternalLevel)
        {
            levelFile = Resources.Load<TextAsset>("Levels/" + levelFilename).text;
        }
        else
        {

        }
        var levelData = levelFile.FromJson<Dictionary<string, object>>();
        var eventData = (List<object>)levelData["events"];
        patterns = (Dictionary<string, object>)levelData["patterns"];
        AddEventsFromList(eventData);
        scrMusic.instance.Play();
    }

    /// <summary>
    /// Adds all the events from the provided list
    /// </summary>
    public void AddEventsFromList(List<object> eventData, int barOffset = 0, double beatOffset = 0)
    {
        foreach (var evData in eventData)
        {
            var ev = (Dictionary<string, object>)evData;
            int repeatAmount;

            try { repeatAmount = (int)ev["repeatAmount"]; }
            catch { repeatAmount = 1; }

            for (var i = 0; i < repeatAmount; i++)
            {
                var newEv = EventFromDict(ev);
                newEv.bar += barOffset;
                newEv.beat += beatOffset;
                newEv.beat += newEv.repeatInterval * i;
                AddEvent(newEv);
            }
        }
    }

    /// <summary>
    /// Adds an event to the list at the right position
    /// </summary>
    public void AddEvent(EventBase ev)
    {
        ev.Create();
        var currentNode = events.First;

        //Look for the last event with a time less than ev.time
        while (currentNode?.Next != null && currentNode.Next.Value.time < ev.time)
            currentNode = currentNode.Next;

        //If no such node was found just put the event at the beginning
        if (currentNode == null || currentNode == events.First && currentNode.Value.time > ev.time)
            events.AddFirst(ev);
        else
            events.AddAfter(currentNode, ev); //otherwise add it after that event
    }

    //todo: move this elsewhere?? maybe as an extension for idictionary if possible hh
    /// <summary>
    /// Converts a dictionary into an object of type T
    /// </summary>
    public static T ObjectFromDictionary<T>(IDictionary<string, object> dict) where T : class
    {
        Type type = typeof(T);
        T result = (T)Activator.CreateInstance(type);
        foreach (var item in dict)
        {
            var key = item.Key;
            var prop = type.GetProperty(key);
            prop?.SetValue(result, item.Value, null);
        }

        return result;
    }

    /// <summary>
    /// Converts an event dictionary into an EventBase
    /// </summary>
    public EventBase EventFromDict(Dictionary<string, object> eventDict)
    {
        var eventType = Type.GetType("event_" + (string)eventDict["type"]); //get the event's type (event_debugMessage, etc)
        var dictToObjMethod = typeof(scrLevel).GetMethod("ObjectFromDictionary", new[] { typeof(IDictionary<string, object>) }); //put the ObjectFromDictionary method into a variable
        var dictToEventMethod = dictToObjMethod.MakeGenericMethod(new[] { eventType }); //make a ObjectFromDictionary<eventType> method
        var result = dictToEventMethod.Invoke(this, new[] { eventDict }); //invoke (call) the method to create the event
        var evResult = (EventBase)result;
        return evResult;
    }

    /// <summary>
    /// Calls PreExecute on all events from the next bar
    /// </summary>
    public void PreExecuteNextBarEvents()
    {
        var ev = events.First;
        while (ev?.Value.bar <= lastPreExecuteBar + 1)
        {
            if (!ev.Value.preExecuted)
                ev.Value.PreRun();
            ev = ev.Next;
        }
    }
}
