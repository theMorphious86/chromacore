﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EventBase
{
    private int _bar;
    private double _beat;

    public bool preExecuted;
    public double time { get; private set; }
    public string type { get; set; }
    public int repeatAmount { get; set; }
    public double repeatInterval { get; set; }
    public int bar
    {
        get => _bar;
        set
        {
            var secondsPerBeat = (60 / scrMusic.instance.bpm);
            _bar = value;
            time = bar * scrMusic.instance.crotchetsPerBar * secondsPerBeat;
            time += beat * secondsPerBeat;
        }
    }
    public double beat
    {
        get => _beat;
        set
        {
            var secondsPerBeat = (60 / scrMusic.instance.bpm);
            _bar += Mathf.FloorToInt((float) (value / scrMusic.instance.crotchetsPerBar)); 
            _beat = value % scrMusic.instance.crotchetsPerBar;
            time = bar * scrMusic.instance.crotchetsPerBar * secondsPerBeat;
            time += beat * secondsPerBeat;
        }
    }


    /// <summary>
    /// Executes the event and repeats it if needed
    /// </summary>
    public void Run()
    {
        Debug.Log($"Executing event {type} at {bar}:{beat}");
        Execute();
    }

    public void PreRun()
    {
        //Debug.Log($"Pre-executing event {type} at {bar}:{beat}");
        PreExecute();
        preExecuted = true;
    }


    protected abstract void Execute();

    protected virtual void PreExecute() { }

    public virtual void Create() { }
}
