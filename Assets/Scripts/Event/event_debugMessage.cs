﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class event_debugMessage : EventBase
{
    public string message { get; set; }

    protected override void Execute()
    {
        Debug.Log(message);
    }
}
