﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class event_playSound : EventBase
{
    public string sound { get; set; }
    public float volume { get; set; } = 100f;
    public float pitch { get; set; } = 1f;

    protected override void Execute() { }

    protected override void PreExecute()
    {
        AudioClip clip = Resources.Load<AudioClip>("Sounds/" + sound);

        Sound snd = scrSoundManager.instance.NewSound(clip);
        snd.SetVolume(volume / 100);
        snd.SetPitch(pitch);
        snd.Schedule((float)(time + scrMusic.instance.startTime));
    }
}
