using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class event_playPattern : EventBase
{
    public string name { get; set; }

    protected override void Execute() { }

    public override void Create()
    {
        var level = scrLevel.instance;
        var pattern = (List<object>) level.patterns[name];
        if (pattern == null)
        {
            Debug.Log($"Pattern {name} does not exist");
            return;
        }
        scrLevel.instance.AddEventsFromList(pattern, bar, beat);
    }
}