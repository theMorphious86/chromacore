﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum KeyBind
{
    Up,
    Down,
    Left,
    Right
}

public class scrSettings : SingletonBehaviour<scrSettings>
{
    // settings
    public float volumeMusic;
    public float volumeSound;

    public Dictionary<KeyBind, KeyCode> keyBinds = new Dictionary<KeyBind, KeyCode>();
    public Dictionary<KeyBind, KeyCode> keyBindsAlt = new Dictionary<KeyBind, KeyCode>();
    public Dictionary<KeyBind, KeyCode> keyBindsController = new Dictionary<KeyBind, KeyCode>();

    // methods
    void Start()
    {
        // init default keybinds
        keyBinds.Add(KeyBind.Up, KeyCode.W);
        keyBindsAlt.Add(KeyBind.Up, KeyCode.UpArrow);

        keyBinds.Add(KeyBind.Down, KeyCode.S);
        keyBindsAlt.Add(KeyBind.Down, KeyCode.DownArrow);

        keyBinds.Add(KeyBind.Left, KeyCode.A);
        keyBindsAlt.Add(KeyBind.Left, KeyCode.LeftArrow);

        keyBinds.Add(KeyBind.Right, KeyCode.D);
        keyBindsAlt.Add(KeyBind.Right, KeyCode.RightArrow);
    }

    public bool GetKey(KeyBind key)
    {
        return Input.GetKey(keyBinds[key]) || Input.GetKey(keyBindsAlt[key]);
    }

    public bool GetKeyDown(KeyBind key)
    {
        return Input.GetKeyDown(keyBinds[key]) || Input.GetKeyDown(keyBindsAlt[key]);
    }

    public bool GetKeyUp(KeyBind key)
    {
        return Input.GetKeyUp(keyBinds[key]) || Input.GetKeyUp(keyBindsAlt[key]);
    }

    public void LoadSettings()
    {

    }

    public void SaveSettings()
    {

    }
}