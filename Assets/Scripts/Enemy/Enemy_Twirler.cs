﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Enemy_Twirler : EnemyBase
{
    public GameObject core;
    private float direction;
    public AudioClip audioClip;

    void Start()
    {
        InitBaseStats();
    }

    void Update()
    {
        var tr = GetComponent<Transform>();
        var playerTr = NearestPlayer().transform;

        Vector3 dir = playerTr.position - tr.position;
        dir = transform.InverseTransformDirection(dir);

        direction = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        direction = direction + tr.eulerAngles.z;

        // debug
        if (Input.GetKeyDown(KeyCode.Q))
        {
            //Sound snd = scrSoundManager.instance.NewSoundAt(gameObject, audioClip);
            //snd.PlaySound();
            Lunge();
        }
    }

    public void Lunge()
    {
        Vector2 velocity = new Vector2
        (
            Mathf.Cos(direction * Mathf.Deg2Rad) * speed,
            Mathf.Sin(direction * Mathf.Deg2Rad) * speed
        );

        GetComponent<Rigidbody2D>().AddForce(velocity, ForceMode2D.Impulse);
        GetComponent<Rigidbody2D>().AddTorque(speed / 3, ForceMode2D.Impulse);
    }
}
