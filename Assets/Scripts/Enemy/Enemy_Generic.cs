﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Generic : EnemyBase
{
    [Header("Enemy specific")]
    public float strengthMin;
    public float strengthMax;

    private float direction;

    void Start()
    {
        Randomize();
    }


    void Update()
    {
        var tr = GetComponent<Transform>();
        var playerTr = NearestPlayer().transform;

        Vector3 dir = playerTr.position - tr.position;
        dir = transform.InverseTransformDirection(dir);

        direction = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        Vector2 velocity = new Vector2
        (
            Mathf.Cos(direction * Mathf.Deg2Rad) * speed,
            Mathf.Sin(direction * Mathf.Deg2Rad) * speed
        );

        GetComponent<Rigidbody2D>().velocity = velocity;
    }

    void Randomize()
    {
        var sprite = GetComponent<SpriteRenderer>();

        // random color
        sprite.color = RandomRed();

        // random strength
        var strength = Random.Range(strengthMin, strengthMax);

        // set stats based on strength
        GetComponent<Transform>().localScale = new Vector2(baseSize, baseSize) * strength; // size
        speed = baseSpeed / (strength / 1.5f); // speed (more strength means less speed)
        health = baseHealth * strength; // health
    }
}
