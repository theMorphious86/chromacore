﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{
    [Header("General")]
    public float baseHealth;
    public float baseSpeed;
    public float baseSize;

    [System.NonSerialized]
    public float health, speed, size, invincibility;

    public GameObject NearestPlayer()
    {
        GameObject[] arr = GameObject.FindGameObjectsWithTag("Player");
        GameObject nearestPlayer = arr[0];
        if (arr.Length == 1) return nearestPlayer;

        float smallestDist = Mathf.Infinity;

        foreach (GameObject obj in arr)
        {
            float dist = Vector3.Distance(obj.transform.position, transform.position);
            if (dist < smallestDist)
            {
                smallestDist = dist;
                nearestPlayer = obj;
            }
        }

        return nearestPlayer;
    }

    public Color RandomRed()
    {
        return Random.ColorHSV(0, 0.02f, 0.75f, 0.95f, 0.75f, 0.90f, 1, 1);
    }

    public void InitBaseStats()
    {
        speed = baseSpeed;
        health = baseHealth;
        size = baseSize;
    }
}
