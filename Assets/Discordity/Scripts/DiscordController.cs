﻿using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class DiscordJoinEvent : UnityEngine.Events.UnityEvent<string> { }

[System.Serializable]
public class DiscordSpectateEvent : UnityEngine.Events.UnityEvent<string> { }

[System.Serializable]
public class DiscordJoinRequestEvent : UnityEngine.Events.UnityEvent<DiscordRpc.JoinRequest> { }

public enum DiscordStates
{
    none,
    inMainMenu,
    inGameLevel,
    inCustomLevel,
    inLevelEditor,
    paused,
    debug
}

public class DiscordController : MonoBehaviour
{
    public DiscordRpc.RichPresence presence;
    public string applicationId;
    public string optionalSteamId;
    public int callbackCalls;
    public DiscordRpc.JoinRequest joinRequest;
    public UnityEngine.Events.UnityEvent onConnect;
    public UnityEngine.Events.UnityEvent onDisconnect;
    public UnityEngine.Events.UnityEvent hasResponded;
    public DiscordJoinEvent onJoin;
    public DiscordJoinEvent onSpectate;
    public DiscordJoinRequestEvent onJoinRequest;

    [Header("Custom")]
    public DiscordStates state;
    private DiscordStates lastState;

    DiscordRpc.EventHandlers handlers;

    void Start()
    {
        DiscordRpc.UpdatePresence(ref presence);
    }

    void Update()
    {
        var scene = SceneManager.GetActiveScene();
        var level = scrLevel.instance;

        if (level.debug)
        {
            state = DiscordStates.debug;
        }
        else if (scene.name == "scnLevel")
        {
            if (level.paused)
            {
                state = DiscordStates.paused;
            }
            else
            {
                if (level.isInternalLevel)
                {
                    state = DiscordStates.inGameLevel;
                }
                else
                {
                    state = DiscordStates.inCustomLevel;
                }
            }
        }

        // update presence
        if (state != lastState)
        {
            lastState = state;
            var message = "";
            switch (state)
            {
                case DiscordStates.none:
                    message = "";
                    break;
                case DiscordStates.inMainMenu:
                    message = "In main menu";
                    break;
                case DiscordStates.inGameLevel:
                    message = "Playing...";
                    break;
                case DiscordStates.inCustomLevel:
                    message = string.Format("Playing {0:LEVEL}", scrLevel.instance.levelFilename);
                    break;
                case DiscordStates.inLevelEditor:
                    message = string.Format("Editing {0:LEVEL}", scrLevel.instance.levelFilename);
                    break;
                case DiscordStates.paused:
                    message = "Paused";
                    break;
                case DiscordStates.debug:
                    message = "Fixing bugs!";
                    break;
            }
            presence.state = message;
            DiscordRpc.UpdatePresence(ref presence);
        }
        DiscordRpc.RunCallbacks();
    }

    public void RequestRespondYes()
    {
        Debug.Log("Discord: responding yes to Ask to Join request");
        DiscordRpc.Respond(joinRequest.userId, DiscordRpc.Reply.Yes);
        hasResponded.Invoke();
    }

    public void RequestRespondNo()
    {
        Debug.Log("Discord: responding no to Ask to Join request");
        DiscordRpc.Respond(joinRequest.userId, DiscordRpc.Reply.No);
        hasResponded.Invoke();
    }

    public void ReadyCallback()
    {
        ++callbackCalls;
        //Debug.Log("Discord: ready");
        onConnect.Invoke();
    }

    public void ForceDisconnect()
    {
        DisconnectedCallback(0, "Forcing disconnection by user");
    }

    public void DisconnectedCallback(int errorCode, string message)
    {
        ++callbackCalls;
        Debug.Log(string.Format("Discord: disconnect {0}: {1}", errorCode, message));
        onDisconnect.Invoke();
    }

    public void ErrorCallback(int errorCode, string message)
    {
        ++callbackCalls;
        Debug.Log(string.Format("Discord: error {0}: {1}", errorCode, message));
    }

    public void JoinCallback(string secret)
    {
        ++callbackCalls;
        Debug.Log(string.Format("Discord: join ({0})", secret));
        onJoin.Invoke(secret);
    }

    public void SpectateCallback(string secret)
    {
        ++callbackCalls;
        Debug.Log(string.Format("Discord: spectate ({0})", secret));
        onSpectate.Invoke(secret);
    }

    public void RequestCallback(ref DiscordRpc.JoinRequest request)
    {
        ++callbackCalls;
        Debug.Log(string.Format("Discord: join request {0}#{1}: {2}", request.username, request.discriminator, request.userId));
        joinRequest = request;
        onJoinRequest.Invoke(request);
    }

    void OnEnable()
    {
        //Debug.Log("Discord: init");
        callbackCalls = 0;

        handlers = new DiscordRpc.EventHandlers();
        handlers.readyCallback = ReadyCallback;
        handlers.disconnectedCallback += DisconnectedCallback;
        handlers.errorCallback += ErrorCallback;
        handlers.joinCallback += JoinCallback;
        handlers.spectateCallback += SpectateCallback;
        handlers.requestCallback += RequestCallback;
        DiscordRpc.Initialize(applicationId, ref handlers, true, optionalSteamId);
    }

    void OnDisable()
    {
        //Debug.Log("Discord: shutdown");
        DiscordRpc.Shutdown();
    }

    void OnDestroy()
    {

    }
}
